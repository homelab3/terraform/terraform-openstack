# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.44.0"
    }
  }
}

resource "openstack_compute_instance_v2" "basic" {
  name            = "basic"
  image_id        = "c4322fc3-df28-40be-9b48-0dfe8323d5a2" 
  flavor_name     = "a2-ram4-disk20-perf1"
  key_pair        = "xps9350key"
  security_groups = ["default"]

  network {
    name = "ext-net1"
  }
}
